
var $LV = jQuery.noConflict();



$LV( window ).resize(function() {
  $LV('#slideWrapper').css('width', $LV('#searchWrapper').width());
});

$LV( document ).ready(function(){

	$LV('#slideWrapper').css('width', $LV('#searchWrapper').width());

//Device Counter
 function countDevices(){
 	var newWidth = $LV('.device:visible').length * 235;
 	//var viewportWidth = $LV('#searchWrapper').width();
 	$LV('#slideWrapper').css('width', $LV('#searchWrapper').width());
 	$LV('#trackersWrapper').css({'width': newWidth,'height':'1000px'});
 	$LV('#trackersWrapper').addClass("compareIt");

 	
 }

//Compare Selected
$LV('body').on('click', '.deviceCompare', function() {

   $LV(this).parent('.device').toggleClass("selected");
   $LV(".fa-star",this).slideToggle();
   $LV(".compareSelected").removeAttr("disabled");
   

});



$LV('.compareSelected').click(function () {

	$LV('.device').css({'width':'235px',});
	$LV('.device:not(.selected)').css('display','none');

	if($LV('.device:visible').length > 0){$LV('#allCompare').css('display','block');}
	$LV('.allData').css('display','block');

	$LV("#fullModule").addClass("comparingActive");

	countDevices();
});


//Compare All
$LV('.compareAll').click(function () {
	
	if($LV("#fullModule").hasClass("comparingActive")){
		$LV('.device').show();
	}

	$LV('.device').css('width','235px');
	$LV('#allCompare').css('display','block');
	$LV('.allData').css('display','block');
	countDevices();
	
	});

//Reset All
$LV('.resetAll').click(function () {
	$LV('.searchButton').removeClass("activeBTN");
	$LV(".compareSelected").prop('disabled', true);
	$LV('.device').removeClass("activeBTN GThdn SThdn HRhdn WRhdn TCNhdn TPAhdn selected");
	$LV('#trackersWrapper').removeClass("compareIt");
	$LV("#fullModule").removeClass("comparingActive");

	$LV('#trackersWrapper').attr('style',"");
	$LV('.device').attr('style',"");
	$LV('#allCompare').attr('style',"");
	$LV('.allData').attr('style',"");
	$LV('#trackersWrapper h3').hide();
	$LV(".fa-star").hide();



	
	});

//Filter Buttons
$LV('.searchButton').click(function () {
	


	$LV(this).toggleClass("activeBTN");
	var searchTerm = $LV(this).attr("data-attr");

	

 	//$LV("button","#buttonWrappper").filter(searchTerm != ('[data-attr="'+searchTerm+'"]')).toggleClass("hidden");
 	$LV(".device", "#trackersWrapper").each(function() {
 		
 		var lookUp = $LV("li", this).hasClass(searchTerm);
 		
 		console.log(this);


 		if(!lookUp){
 			
 				$LV(this).toggleClass(searchTerm+"hdn");
 			
 			
 			
 		}
 		
 	});
 
 	
});
	
	
})