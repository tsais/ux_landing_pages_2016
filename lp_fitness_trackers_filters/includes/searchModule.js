
var $LV = jQuery.noConflict();

function buildSearchButtons(searchModule, searchItemWrapper, searchArray){

	for(ft=0; ft < searchArray.length; ft++){

		$LV(searchModule).append('<button class="searchButton" href="#" data-attr="'+searchArray[ft]+'">'+searchArray[ft]+'</button>');

	}
}

$LV('body').on('click', '.searchButton', function () {
	

	$LV(this).toggleClass("activeBTN");
	var searchTerm = $LV(this).attr("data-attr");

 	$LV("div",searchItemWrapper).filter(searchTerm != ('[data-attr="'+searchTerm+'"]')).toggleClass("hidden");
 	

});


$LV( document ).ready(function(){

	buildSearchButtons("#searchModule","#searchItemWrapper",["Treadmill Compatabile","Music Control","Tracks Steps","Mobile App"]);
	
})