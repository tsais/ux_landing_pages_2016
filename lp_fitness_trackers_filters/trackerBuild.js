/*
JavaScript Document

FinalVadaszy
11/5/15

*/


var $LV=jQuery.noConflict();


var teamOut = function(cellID){
	$LV(cellID).animate({opacity: "0", top:0,right:-1220},350,function() {$LV(this).css("display","none");});

	$LV(".NFLteam .mainIMG").show();
	$LV(this).removeClass("QX");
}
var teamIn = function(cellID){
	//$LV(cellID).animate({opacity: 1.0,top:0,left:-1220},350,function() {$LV(this).removeAttr("style").css("display","block");});
	$LV(cellID).animate({opacity: 1.0,top:0,left:-1220},350,function() {$LV(this).removeAttr("style").show();});

	$LV(".NFLteam .mainIMG").hide();
	$LV(cellID + " .teamDetails ").css('display','block');
	$LV(this).addClass("QX");


}

var activeBTN = function(currentBTN){

 $LV(".cta-button").removeClass(  "sa-Dgray-bg" );
 $LV(currentBTN).toggleClass(  "sa-Dgray-bg" ,"sa-red-bg");

 //$LV(currentBTN).css("background-color","black");
}



$LV( document ).ready(function(){

$LV("img").unveil();
	
$LV(".Allbtn").addClass(  "sa-Dgray-bg" );

//ALL
$LV( '.Allbtn' ).click(function() {

	activeBTN(this);

	$LV(".AFC").removeAttr("style");
	$LV(".NFC").removeAttr("style");
	$LV("#NFLteams div").fadeIn("slow");
	$LV(".teamDetails").css('display','none');
	$LV(".NFLteam .mainIMG").show();
})

//AFC 
$LV( '.AFCbtn' ).click(function() {
	
	activeBTN(this);
	
	$LV(".AFC").removeAttr("style").css('display','block');
	$LV(".NFC").css('display','none');
	$LV(".teamDetails").css('display','none');
	$LV(".NFLteam .mainIMG").show();
	
	
	
})
$LV( '.AFC-North' ).click(function() {
	activeBTN(this);
	$LV(".AFCbtn").addClass(  "sa-Dgray-bg" );
	teamOut("#NFLteams div:not(.AFC.North)");
	teamIn("#NFLteams .AFC.North");
	$LV("img").trigger("unveil");
	
	
	
})
$LV( '.AFC-South' ).click(function() {
	activeBTN(this);
	$LV(".AFCbtn").addClass(  "sa-Dgray-bg" );
	teamOut("#NFLteams div:not(.AFC.South)");
	teamIn("#NFLteams .AFC.South");
	$LV("img").trigger("unveil");
	
	
})
$LV( '.AFC-East' ).click(function() {
	activeBTN(this);
	$LV(".AFCbtn").addClass(  "sa-Dgray-bg" );
	teamOut("#NFLteams div:not(.AFC.East)");
	teamIn("#NFLteams .AFC.East");
	$LV("img").trigger("unveil");
	
})
$LV( '.AFC-West' ).click(function() {
	activeBTN(this);
	$LV(".AFCbtn").addClass(  "sa-Dgray-bg" );
	teamOut("#NFLteams div:not(.AFC.West)");
	teamIn("#NFLteams .AFC.West");
	$LV("img").trigger("unveil");
	
})

//NFC 
$LV( '.NFCbtn' ).click(function() {

	activeBTN(this);

	$LV(".NFC").removeAttr("style").css('display','block');
	$LV(".AFC").css('display','none');
	$LV(".teamDetails").css('display','none');
	$LV(".NFLteam .mainIMG").show();
	
	
})
$LV( '.NFC-North' ).click(function() {
	activeBTN(this);
	$LV(".NFCbtn").addClass(  "sa-Dgray-bg" );
	teamOut("#NFLteams div:not(.NFC.North)");
	teamIn("#NFLteams .NFC.North");
	$LV("img").trigger("unveil");
	
})
$LV( '.NFC-South' ).click(function() {
	activeBTN(this);
	$LV(".NFCbtn").addClass(  "sa-Dgray-bg" );
	teamOut("#NFLteams div:not(.NFC.South)");
	teamIn("#NFLteams .NFC.South");
	$LV("img").trigger("unveil");
	
})
$LV( '.NFC-East' ).click(function() {
	activeBTN(this);
	$LV(".NFCbtn").addClass(  "sa-Dgray-bg" );
	teamOut("#NFLteams div:not(.NFC.East)");
	teamIn("#NFLteams .NFC.East");
	$LV("img").trigger("unveil");
	
})
$LV( '.NFC-West' ).click(function() {
	activeBTN(this);
	$LV(".NFCbtn").addClass(  "sa-Dgray-bg" );
	teamOut("#NFLteams div:not(.NFC.West)");
	teamIn("#NFLteams .NFC.West");
	$LV("img").trigger("unveil");
	
})

//Search Bar method

$LV( '.closeOut' ).click(function() {
	$LV('#teamSearch').val("");
	$LV("#NFLteams div").css('display','block');
	$LV(".teamDetails").css('display','none');
	$LV('.closeOut').css("display","none");
	$LV(".AFC").removeAttr("style");
	$LV(".NFC").removeAttr("style");
	$LV(".NFLteam .mainIMG").show();
	
})

var xButton = function(isEmpty){

	var addBtn = 0;
	if( !addBtn && isEmpty > 0){

    	$LV('.closeOut').css("display","inline-block");
    }else{
    	$LV('.closeOut').css("display","none");
    }
}



$LV('#teamSearch').on('input', function() { 

	$LV(".NFLteam").css({"opacity":"inherit","top":"inherit","right":"inherit"});

	

    var currentSearchTerm = $LV(this).val(); // get the current value of the input field.
    var currentLetters = currentSearchTerm.length;
   
   	xButton(currentLetters);

   	if(currentLetters <= 0){
   	$LV(".AFC").removeAttr("style");
	$LV(".NFC").removeAttr("style");
	$LV(".teamDetails").css('display','none');
   	}

  

    $LV("#NFLteams div").each(function(){

    	//var currentClass = $LV(this).attr("class");
    	var currentClass = $LV(".teamFilter",this).text();

    	
    	if (currentClass.toLowerCase().indexOf(currentSearchTerm.toLowerCase()) <= 0){
    		$LV(this).css('display','none');
    		$LV(this).removeClass("QX");
    		
    	}
    	if (currentClass.toLowerCase().indexOf(currentSearchTerm.toLowerCase()) >= 0){
    		$LV(this).css('display','block');
    		$LV(this).addClass("QX");
    		
    		
    	}


    })
     	var visibleTeams = $LV("#NFLteams .QX").length;
    	if(visibleTeams <= 4 ){
    		$LV(".teamDetails").css('display','block');
    		$LV(".NFLteam .mainIMG").hide();
    		$LV("img").trigger("unveil");
    	}else if((visibleTeams == 2 )){
    		$LV(".teamDetails").css('display','block');
    		$LV("img").trigger("unveil");
    		$LV(".NFLteam .mainIMG").hide();
    	}else if((visibleTeams == 1 )){
    		$LV(".teamDetails").css('display','block');
  	$LV("img").trigger("unveil");
    		$LV(".NFLteam .mainIMG").hide();
    	}else{
    		$LV(".teamDetails").css('display','none');
    		$LV(".NFLteam .mainIMG").show();
    	}
    
});



	













})