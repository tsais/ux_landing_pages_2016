var endcatch = 0;

(function (window) {
	function Banner(){
		this.init();
	}

	Banner.prototype.init = function(){
		this.__width = 300;
		this.__height = 250;
		this.start();
	};

	Banner.prototype.render = function(){
		this.defineElements();
		this.positionElements();
		this.defineInteraction();
		this.start();
		this.run();
	};

	Banner.prototype.defineElements = function(){
		//-------------------------------------------------------------------------------
		//Do Not Edit
		//-------------------------------------------------------------------------------
		this.__container = document.getElementById("container");
		this.__banner = document.getElementById("banner");
		this.__border = document.getElementById("border");
		this.__content = document.getElementById("content");
		this.__cta = document.getElementById("cta");
		//-------------------------------------------------------------------------------
		//End Do Not Edit
		//-------------------------------------------------------------------------------
	};

	Banner.prototype.positionElements = function(){
		var w = this.__width;
		var h = this.__height;
		var stroke = 1;

		TweenMax.set("#product1", {y:-209});
		TweenMax.set("#product2", {y:-209});
		TweenMax.set("#product3", {y:-209});
		TweenMax.set("#graybg", {y:209});
		TweenMax.set("#frame5copy4", {y:-300});

		TweenMax.set(this.__container, {width:w, height:h, top:0, left:0});
		TweenMax.set(this.__banner, {width:w, height:h, top:0, left:0});
		TweenMax.set(this.__content, {width:w, height:h, top:0, left:0});
		TweenMax.set(this.__border, {width:w-stroke*2, height:h-stroke*2, top:0, left:0});
	};

	Banner.prototype.defineInteraction = function(){
		var banner = this;
		var offset = 4;
		this.__container.onclick = function(){ banner.clickThrough(); };
	};

	Banner.prototype.onMouseOver = function(){

	};

	Banner.prototype.onMouseOut = function(){

	};

	Banner.prototype.playCTA = function(){
		TweenMax.to("#footer_hover",0,{opacity:1});
		TweenMax.to("#footer_hover",0,{opacity:0, delay:.5});
	};

	Banner.prototype.clickThrough = function(){
		trace("click through: " + window.clickTag);
		window.open(window.clickTag);
	};

	//-------------------------------------------------------------------------

	Banner.prototype.start = function(){
		this.__start = new Date();
	};

	Banner.prototype.end = function(){
		var now = new Date();
		var time = now.getTime() - this.__start.getTime();
		trace("total run time = " + time/1000 + " seconds");
	};

	//-------------------------------------------------------------------------
	// User Created Functions Start
	//-------------------------------------------------------------------------

	Banner.prototype.run = function(){
		var banner = this;

		setTimeout(function(){banner.changeToFrame1();}, 0);
		setTimeout(function(){banner.changeToFrame2();}, 3000);
		setTimeout(function(){banner.changeToFrame3();}, 5000);
		setTimeout(function(){banner.changeToFrame4();}, 7000);
		setTimeout(function(){banner.changeToFrame5();}, 9000);

		setTimeout(function(){banner.playCTA();}, 14500);
	};

	Banner.prototype.changeToFrame1 = function(){
		TweenMax.to("#frame1copy1", 0, {opacity:1, ease:Power0.easeNone, delay:.5});

		TweenMax.to("#frame1copy1", 0, {opacity:0, ease:Power0.easeNone, delay:1.25});
		TweenMax.to("#frame1copy2", 0, {opacity:1, ease:Power0.easeNone, delay:1.25});

		TweenMax.to("#frame1copy2", 0, {opacity:0, ease:Power0.easeNone, delay:2});
		TweenMax.to("#frame1copy3", 0, {opacity:1, ease:Power0.easeNone, delay:2});
	}

	Banner.prototype.changeToFrame2 = function(){
		TweenMax.to("#product1", .25, {y:0, ease:Power0.easeNone});
	}

	Banner.prototype.changeToFrame3 = function(){
		TweenMax.to("#product2", .25, {y:0, ease:Power0.easeNone});
	}

	Banner.prototype.changeToFrame4 = function(){
		TweenMax.to("#product3", .25, {y:0, ease:Power0.easeNone});
	}

	Banner.prototype.changeToFrame5 = function(){
		TweenMax.to("#graybg", .25, {y:0, ease:Power0.easeNone});

		TweenMax.to("#frame5copy2", .25, {rotationX:90, delay:2});
		TweenMax.to("#frame5copy3", .25, {rotationX:0, delay:2.25});
		
		TweenMax.to("#frame5copy1", .5, {y:300, delay:4.25});
		TweenMax.to("#frame5copy3", .5, {y:300, delay:4.25});
		TweenMax.to("#frame5copy4", .5, {y:0, delay:4});
	}
	//-------------------------------------------------------------------------
	// User Created Functions End
	//-------------------------------------------------------------------------

	window.Banner = Banner;

}(window));

//-------------------------------------------------------------------------
// Global functions
//-------------------------------------------------------------------------

function trace(s){
	console.log(s);
}

function timeout( _delay, _func ){
	var to = setTimeout(function(){_func();}, _delay);
	return to;
}

Function.prototype.inheritsFrom = function( superClass ){
	this.prototype = new superClass();
	this.prototype.constructor = this;
	this.prototype.sooper = superClass.prototype;
	return this;
};
