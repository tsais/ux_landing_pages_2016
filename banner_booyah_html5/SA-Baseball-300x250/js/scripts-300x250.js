var endcatch = 0;

(function (window) {
	function Banner(){
		this.init();
	}

	Banner.prototype.init = function(){
		this.__width = 300;
		this.__height = 250;
		this.start();
	};

	Banner.prototype.render = function(){
		this.defineElements();
		this.positionElements();
		this.defineInteraction();
		this.start();
		this.run();
	};

	Banner.prototype.defineElements = function(){
		//-------------------------------------------------------------------------------
		//Do Not Edit
		//-------------------------------------------------------------------------------
		this.__container = document.getElementById("container");
		this.__banner = document.getElementById("banner");
		this.__border = document.getElementById("border");
		this.__content = document.getElementById("content");
		this.__cta = document.getElementById("cta");
		//-------------------------------------------------------------------------------
		//End Do Not Edit
		//-------------------------------------------------------------------------------
	};

	Banner.prototype.positionElements = function(){
		var w = this.__width;
		var h = this.__height;
		var stroke = 1;

		TweenMax.set("#frame1copy1", {x:-300});
		TweenMax.set("#frame1copy2", {x:-300});

		TweenMax.set("#frame2copy1", {y:-150});

		TweenMax.set("#frame4copy1", {y:-150});

		TweenMax.set("#product1a", {y:300});
		TweenMax.set("#product1b", {y:300});
		TweenMax.set("#product1c", {y:300});

		TweenMax.set("#baseball2", {y:-300, x:-300});


		TweenMax.set(this.__container, {width:w, height:h, top:0, left:0});
		TweenMax.set(this.__banner, {width:w, height:h, top:0, left:0});
		TweenMax.set(this.__content, {width:w, height:h, top:0, left:0});
		TweenMax.set(this.__border, {width:w-stroke*2, height:h-stroke*2, top:0, left:0});
	};

	Banner.prototype.defineInteraction = function(){
		var banner = this;
		var offset = 4;
		this.__container.onclick = function(){ banner.clickThrough(); };




	};

	Banner.prototype.onMouseOver = function(){

	};

	Banner.prototype.onMouseOut = function(){

	};

	Banner.prototype.playCTA = function(){

		TweenMax.to("#footer_hover",0,{opacity:1});
		TweenMax.to("#footer_hover",0,{opacity:0, delay:.5});
	};

	Banner.prototype.clickThrough = function(){
		trace("click through: " + window.clickTag);
		window.open(window.clickTag);
	};

	//-------------------------------------------------------------------------

	Banner.prototype.start = function(){
		this.__start = new Date();
	};

	Banner.prototype.end = function(){
		var now = new Date();
		var time = now.getTime() - this.__start.getTime();
		trace("total run time = " + time/1000 + " seconds");
	};

	//-------------------------------------------------------------------------
	// User Created Functions Start
	//-------------------------------------------------------------------------

	Banner.prototype.run = function(){
		var banner = this;

		setTimeout(function(){banner.changeToFrame1();}, 0);
		setTimeout(function(){banner.changeToFrame2();}, 2500);
		setTimeout(function(){banner.changeToFrame3();}, 5500);
		setTimeout(function(){banner.changeToFrame4();}, 11500);
		setTimeout(function(){banner.changeToFrame5();}, 13500);

		setTimeout(function(){banner.playCTA();}, 14500);
	};

	Banner.prototype.changeToFrame1 = function(){
		TweenMax.to("#frame1copy1", 1, {x:0, ease:Power4.easeOut, delay:1});
		TweenMax.to("#frame1copy2", 1, {x:0, ease:Power4.easeOut, delay:1.2});
	}

	Banner.prototype.changeToFrame2 = function(){
		TweenMax.to("#baseball3", .7, {opacity:0, scaleX:50, scaleY:50, top:50, left:100, ease:Power4.easeIn});
		TweenMax.to("#baseball", .7, {opacity:1, scaleX:50, scaleY:50, top:50, left:100, ease:Power4.easeIn});

		TweenMax.to("#frame2copy1", .5, {y:0, delay:.8});
		TweenMax.to("#product1a", .5, {y:0, delay:.8});
		TweenMax.to("#product1b", .5, {y:0, delay:.8});
		TweenMax.to("#product1c", .5, {y:0, delay:.8});
	}

	Banner.prototype.changeToFrame3 = function(){
		TweenMax.to("#frame2copy1", .25, {rotationX:90, delay:0});
		TweenMax.to("#frame3copy1", .25, {rotationX:0, delay:.25});

		TweenMax.to("#product1a", 0, {opacity:0, delay:2});
		TweenMax.to("#product1b", 0, {opacity:0, delay:2});
		TweenMax.to("#product1c", 0, {opacity:0, delay:2});

		TweenMax.to("#product1a", 0, {y:300, opacity:1, delay:2.2});
		TweenMax.to("#product1b", 0, {y:300, opacity:1, delay:2.2});
		TweenMax.to("#product1c", 0, {y:300, opacity:1, delay:2.2});

		TweenMax.to("#logo1", 0, {opacity:1, ease:Power0.easeNone, delay:2});

		TweenMax.to("#logo1", 0, {opacity:0, ease:Power0.easeNone, delay:2.75});
		TweenMax.to("#logo2", 0, {opacity:1, ease:Power0.easeNone, delay:2.75});

		TweenMax.to("#logo2", 0, {opacity:0, ease:Power0.easeNone, delay:3.5});
		TweenMax.to("#logo3", 0, {opacity:1, ease:Power0.easeNone, delay:3.5});

		TweenMax.to("#logo3", 0, {opacity:0, ease:Power0.easeNone, delay:4.25});
		TweenMax.to("#logo4", 0, {opacity:1, ease:Power0.easeNone, delay:4.25});

		TweenMax.to("#logo4", 0, {opacity:0, ease:Power0.easeNone, delay:5});
		TweenMax.to("#logo5", 0, {opacity:1, ease:Power0.easeNone, delay:5});
	}

	Banner.prototype.changeToFrame4 = function(){

		TweenMax.to("#logo5", .6, {y:250, ease:Power2.easeIn});
		TweenMax.to("#frame3copy1", .6, {y:-250,ease:Power2.easeIn});
		TweenMax.to("#frame4copy1", .6, {y:0,ease:Power2.easeInOut});

		TweenMax.to("#product1a", .5, {y:0, delay:.2});
		TweenMax.to("#product1b", .5, {y:0, delay:.2});
		TweenMax.to("#product1c", .5, {y:0, delay:.2});

		TweenMax.to("#product1a", .25, {rotationY:90, delay:1.5});
		TweenMax.to("#product1b", .25, {rotationY:90, delay:1.6});
		TweenMax.to("#product1c", .25, {rotationY:90, delay:1.7});

		TweenMax.to("#product2a", .25, {rotationY:0, delay:1.75});
		TweenMax.to("#product2b", .25, {rotationY:0, delay:1.85});
		TweenMax.to("#product2c", .25, {rotationY:0, delay:1.95});
	}

	Banner.prototype.changeToFrame5 = function(){
		TweenMax.to("#baseball2", .6, {y:0, x:0,ease:Power2.easeInOut});
	}
	//-------------------------------------------------------------------------
	// User Created Functions End
	//-------------------------------------------------------------------------

	window.Banner = Banner;

}(window));

//-------------------------------------------------------------------------
// Global functions
//-------------------------------------------------------------------------

function trace(s){
	console.log(s);
}

function timeout( _delay, _func ){
	var to = setTimeout(function(){_func();}, _delay);
	return to;
}

Function.prototype.inheritsFrom = function( superClass ){
	this.prototype = new superClass();
	this.prototype.constructor = this;
	this.prototype.sooper = superClass.prototype;
	return this;
};
