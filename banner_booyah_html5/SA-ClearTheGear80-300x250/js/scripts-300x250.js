var endcatch = 0;

(function (window) {
	function Banner(){
		this.init();
	}

	Banner.prototype.init = function(){
		this.__width = 300;
		this.__height = 250;
		this.start();
	};

	Banner.prototype.render = function(){
		this.defineElements();
		this.positionElements();
		this.defineInteraction();
		this.start();
		this.run();
	};

	Banner.prototype.defineElements = function(){
		//-------------------------------------------------------------------------------
		//Do Not Edit
		//-------------------------------------------------------------------------------
		this.__container = document.getElementById("container");
		this.__banner = document.getElementById("banner");
		this.__border = document.getElementById("border");
		this.__content = document.getElementById("content");
		this.__cta = document.getElementById("cta");
		//-------------------------------------------------------------------------------
		//End Do Not Edit
		//-------------------------------------------------------------------------------
	};

	Banner.prototype.positionElements = function(){
		var w = this.__width;
		var h = this.__height;
		var stroke = 1;

		TweenMax.set("#frame1copy1", {y:250});
		TweenMax.set("#frame1copy2", {y:250});
		TweenMax.set("#frame1copy3", {y:250});
		TweenMax.set("#frame3copy1", {y:-80});
		TweenMax.set("#frame3copy2", {x:-150});
		TweenMax.set("#frame2copy1", {y:-10});
		TweenMax.set("#frame2copy2", {y:-10});
		TweenMax.set("#frame2copy3", {x:-5});

	
		TweenMax.set("#small_tag", {x:300});

		TweenMax.set(this.__container, {width:w, height:h, top:0, left:0});
		TweenMax.set(this.__banner, {width:w, height:h, top:0, left:0});
		TweenMax.set(this.__content, {width:w, height:h, top:0, left:0});
		TweenMax.set(this.__border, {width:w-stroke*2, height:h-stroke*2, top:0, left:0});
	};

	Banner.prototype.defineInteraction = function(){
		var banner = this;
		var offset = 4;
		this.__container.onclick = function(){ banner.clickThrough(); };
	};

	Banner.prototype.onMouseOver = function(){

	};

	Banner.prototype.onMouseOut = function(){

	};

	Banner.prototype.playCTA = function(){
		TweenMax.to("#footer_hover",0,{opacity:1});
		TweenMax.to("#footer_hover",0,{opacity:0, delay:.5});
	};

	Banner.prototype.clickThrough = function(){
		trace("click through: " + window.clickTag);
		window.open(window.clickTag);
	};

	//-------------------------------------------------------------------------

	Banner.prototype.start = function(){
		this.__start = new Date();
	};

	Banner.prototype.end = function(){
		var now = new Date();
		var time = now.getTime() - this.__start.getTime();
		trace("total run time = " + time/1000 + " seconds");
	};

	//-------------------------------------------------------------------------
	// User Created Functions Start
	//-------------------------------------------------------------------------

	Banner.prototype.run = function(){
		var banner = this;

		setTimeout(function(){banner.changeToFrame1();}, 0);
		setTimeout(function(){banner.changeToFrame2();}, 3000);
		setTimeout(function(){banner.changeToFrame3();}, 7500);
		setTimeout(function(){banner.changeToFrame4();}, 10000);
		setTimeout(function(){banner.changeToFrame5();}, 13500);

		setTimeout(function(){banner.playCTA();}, 14500);
	};

	Banner.prototype.changeToFrame1 = function(){
		TweenMax.to("#frame1copy1", 1, {y:0, ease:Power4.easeIn, delay:.1});
		TweenMax.to("#frame1copy2", 1, {y:0, ease:Power4.easeIn, delay:.5});
		TweenMax.to("#frame1copy3", 1, {y:0, ease:Power4.easeIn, delay:.9});
	}

	Banner.prototype.changeToFrame2 = function(){
		TweenMax.to("#frame1copy1", .1, {opacity:0, delay:2.5});
		TweenMax.to("#frame1copy2", .1, {opacity:0, delay:2.5});
		TweenMax.to("#frame1copy3", .1, {opacity:0, delay:2.5});

		TweenMax.to("#large_tag", 5, {x:720, ease:Power1.easeInOut});

		TweenMax.to("#frame2copy1", .1, {opacity:1, delay:2.5});
		TweenMax.to("#frame2copy2", .1, {opacity:1, delay:2.5});
		TweenMax.to("#frame2copy3", .1, {opacity:1, delay:2.5});
	}

	Banner.prototype.changeToFrame3 = function(){

		TweenMax.to("#small_tag", 3, {x:0, ease:Power4.easeInOut});
		TweenMax.to("#frame4copy2", 3, {x:-160, ease:Power4.easeInOut});
		TweenMax.to("#frame4copy1container", 3, {x:-150, ease:Power4.easeInOut});
		TweenMax.to("#frame2copy1", 1, {y:-38, x:45, scaleX:.75, scaleY:.75, ease:Power4.easeInOut, delay:.8});
		TweenMax.to("#frame2copy2", 1, {y:-55, x:45, scaleX:.75, scaleY:.75, ease:Power4.easeInOut, delay:.8});
		TweenMax.to("#frame2copy3", 1, {y:-35, x:10, scaleX:.75, scaleY:.75, ease:Power4.easeInOut, delay:.8});

		TweenMax.to("#frame3copy1", 1, {y:0, ease:Power4.easeIn, delay:2.2});
		TweenMax.to("#frame2copy1", 1, {y:12, x:0, ease:Power4.easeIn, delay:2.2});

		TweenMax.to("#frame4copy1", 1, {x:-160, ease:Power4.easeInOut, delay:4});
		TweenMax.to("#frame4copy2", 1, {x:0, ease:Power4.easeInOut, delay:4.5});
	}

	Banner.prototype.changeToFrame4 = function(){

	}

	Banner.prototype.changeToFrame5 = function(){

	}
	//-------------------------------------------------------------------------
	// User Created Functions End
	//-------------------------------------------------------------------------

	window.Banner = Banner;

}(window));

//-------------------------------------------------------------------------
// Global functions
//-------------------------------------------------------------------------

function trace(s){
	console.log(s);
}

function timeout( _delay, _func ){
	var to = setTimeout(function(){_func();}, _delay);
	return to;
}

Function.prototype.inheritsFrom = function( superClass ){
	this.prototype = new superClass();
	this.prototype.constructor = this;
	this.prototype.sooper = superClass.prototype;
	return this;
};
