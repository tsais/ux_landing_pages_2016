
window.onload = function() {

	var copy2 = document.getElementById("copy2");
	var copy3 = document.getElementById("copy3");
	var cta = document.getElementById("cta");
	var logo = document.getElementById("logo");
	var basketball = document.getElementById("basketball");
	var tennisball = document.getElementById("tennisball");
	var football = document.getElementById("football");
	var baseball = document.getElementById("baseball");
	var mass1 = document.getElementById("mass1");
	var mass2 = document.getElementById("mass2");
	var mass3 = document.getElementById("mass3");
	var mass4 = document.getElementById("mass4");
	var mass5 = document.getElementById("mass5");
	var mass6 = document.getElementById("mass6");
	var mass7 = document.getElementById("mass7");
	var mass8 = document.getElementById("mass8");
	var pile = document.getElementById("pile");
	var leftball = document.getElementById("leftball");
	var bigball = document.getElementById("bigball");
	var booyahbannerwrapper = document.getElementById("booyahbannerwrapper");



	var runAnimation = function() {

		timeLine = new TimelineMax();

		timeLine.to(logo, 0, {top:0, x:0, y:-500, left: 0, opacity: 1, scale: 1},0)
			.to(copy2, 0, {x: 0, y:0,top: 0, left: 0, opacity: 0, scale: 0.3, rotation: 0})
			.to(copy3, 0, {top: 0, left: 0, opacity: 0, scale: 0.3})
			.to(cta, 0, {top: 0, left: -20, opacity: 0, scale: 1})
			.to(mass1, 0, {x: 0, y:0,top: -800, left: -400, opacity: 1,scale: .9})
			.to(mass2, 0, {x: 0, y:0,top: -500, left: -400, opacity: 1})
			.to(mass3, 0, {x: 0, y:0,top: -600, left: -400, opacity: 1})
			.to(mass4, 0, {x: 0, y:0,top: -400, left: -400, opacity: 1})
			.to(mass5, 0, {x: 0, y:0,top: -300, left: -400, opacity: 1, rotation: 180})
			.to(mass6, 0, {x: 0, y:0,top: -500, left: -400, opacity: 1,rotation: 180})
			.to(mass7, 0, {x: 0, y:0,top: -400, left: -400, opacity: 1,rotation: 180})
			.to(mass8, 0, {x: 0, y:0,top: -300, left: -400, opacity: 1,rotation: 180})
			.to(basketball, 0, {x: 0, y:0,top: -110, left: 50, opacity: 1})
			.to(bigball, 0, {x: 0, y:0,top: -800, left: -800, opacity: 1})
			.to(baseball, 0, {x: 0, y:0,top: -50, left: -150, opacity: 1})
			.to(football, 0, {x: 0, y:0,top: 120, left: -150, opacity: 1})
			.to(tennisball, 0, {x: 0, y:0,top: 0, left: -150, opacity: 1})
			.to(pile, 0, {x: 0, y:0,top: 300, left: 0, opacity: 1})
			.to(logo, .5, {y:0, scale: 1})
			.to(logo, .5, {y:-200, x: 0, scale: .95 })
			.to(tennisball, 1.2, {y:1200, x: 800, rotation: 90,ease: Power0.easeNone},.5)
			.to(football, 1.2, {y:1200, x: 800,ease: Power0.easeNone},'-=1.2')
			.to(basketball, 1.2, {y:1200, x: 800, rotation: 90,ease: Power0.easeNone},'-=.5')
			.to(baseball, 1.2, {y:1200, x: 800, rotation: 90,ease: Power0.easeNone},'-=1')
			.to(mass1, 1.2, {y:1200, x: 800,ease: Power0.easeNone},1.5)
			.to(mass2, 1.2, {y:1200, x: 800,ease: Power0.easeNone},2)
			.to(mass3, 1, {y:1200, x: 800,ease: Power0.easeNone},1.5)
			.to(mass4, 1.2, {y:1200, x: 800,ease: Power0.easeNone},2)
			.to(mass5, 1, {y:1200, x: 800,ease: Power0.easeNone},1.5)
			.to(mass6, 1.2, {y:1200, x: 800,ease: Power0.easeNone},2)
			.to(mass7, 1, {y:1200, x: 800,ease: Power0.easeNone},1.5)
			.to(mass8, 1.2, {y:1200, x: 800,ease: Power0.easeNone},2)
			.to(bigball, 1.2, {y:2000, x: 1500,ease: Power0.easeNone},2.5)
			.to(booyahbannerwrapper, 0, {css:{background: "#ffffff"}},2.95)
			.to(copy2, .5, {scale: 1, opacity: 1, ease: Elastic.easeOut.config(1.5,0.90)},3.2)
			.to(copy2, 1, {x:2200, y:1800,scale: 40, rotation: -90, ease: Power2.easeIn},7.2)
			.to(booyahbannerwrapper, 0, {css:{background: "#ee2e24"}})
			.to(copy2, 0, {opacity:0})
			.to(copy3, .5, {scale: 1, opacity: 1, ease: Elastic.easeOut.config(1.5,0.90)},7.7)
			.to(cta, .5, {x: 20,opacity: 1,},3.4)
			.to(pile, .5, {y: -300, ease: Power2.easeOut},8.2);
			

		return timeLine;


	};

	var closeAnimation = function() {

		var timeLine = new TimelineMax();

		    timeLine.to(copy3, .5, {opacity: 0, scale: .3,ease: Elastic.easeIn.config(1.5,0.90)}, 0)
			.to(logo, .5, {opacity: 0, scale: .3,ease: Elastic.easeIn.config(1.5,0.90)}, 0)
			.to(cta, .5, {opacity: 0, scale: .3,ease: Elastic.easeIn.config(1.5,0.90)}, 0)
			.to(pile, 1, {y: 300, ease: Power2.easein}, .5)
			.to(leftball, 1, {x: 0,y: 0, ease: Power2.easeOut},.5);

		return timeLine;

	};

	var mainTimeline = new TimelineMax();

	mainTimeline.add(runAnimation, 0)
				.add(closeAnimation, 12)
				.add(runAnimation, 13.5);
				


};


	







		




	