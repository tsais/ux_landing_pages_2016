Page is here:

http://www.sportsauthority.com/shop/index.jsp?categoryId=11487485



Here are the final files for MRA. Let me know if I missed something or you need layered PSD.

Dynamic text:

color for all text: #4c4e4d
"Donate Today" and "The Facts" body text:
Arial, regular, size 16pt, leading (line height) 22pt 

"Did You Know" text:
Headline: 
Arial, bold, size 20pt, leading (line height) 22pt 
Body: 
Arial, regular, size 14pt, leading (line hight) 20pt

"Dig a little Deeper" text:
Arial, regular, size 16, leading (line height) 22pt

"Thank You" text:
Arial, bold, size 20pt

Disclaimer text under MRA and SA logos:
Arial, regular, size 10pt, leading 12pt

Let me know if you have questions.

Lora