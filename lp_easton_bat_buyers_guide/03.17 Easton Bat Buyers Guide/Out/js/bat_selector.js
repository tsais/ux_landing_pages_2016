


<script type="text/javascript">

function calculateBat() {

var heightInput = document.getElementById("inches");
var height = heightInput.value;

// var height = document.getElementById("inches").value;
var weight = document.getElementById("pounds").value;
var errorHolder = document.getElementsByClassName("input_error")[0];
var suggested = document.getElementById("suggested");

if (isNaN(height)){
	errorHolder.innerHTML = "Height is an invalid entry! Please enter only a number.";
}
if (isNaN(weight)){
	errorHolder.innerHTML = "Weight is an invalid entry! Please enter only a number.";
}



	if (height <= 40 && weight <= 60) {
		// document.write("26 &rdquo; ");
		suggested.value = "26\" ";
	}
	else if (height <= 40 && weight <= 100) {
		// document.write("27 &rdquo; ");
		suggested.value = "27\" ";
	}
	else if (height <= 40 && weight <= 150) {
		//document.write("28 &rdquo; ");
		suggested.value = "28\" ";
	}
	else if (height <= 40 && weight <= 180) {
		// document.write("29 &rdquo; ");
		suggested.value = "29\" ";
	}
	else if (height <= 40 && weight > 180) {
		// document.write("30 &rdquo; ");
		suggested.value = "30\" ";
	}
	else if (height <= 44 && weight <= 70) {
		// document.write("27 &rdquo; ");
		suggested.value = "27\" ";
	}
	else if (height <= 44 && weight <= 100) {
		// document.write("28 &rdquo; ");
		suggested.value = "28\" ";
	}
	else if (height <= 44 && weight <= 180) {
		// document.write("29 &rdquo; ");
		suggested.value = "29\" ";
	}
	else if (height <= 44 && weight > 180) {
		// document.write("30 &rdquo; ");
		suggested.value = "30\" ";
	}
	else if (height <= 48 && weight <= 80) {
		// document.write("28 &rdquo; ");
		suggested.value = "28\" ";
	}
	else if (height <= 48 && weight <= 130) {
		// document.write("29 &rdquo; ");
		suggested.value = "29\" ";
	}
	else if (height <= 48 && weight <= 180) {
		// document.write("30 &rdquo; ");
		suggested.value = "30\" ";
	}
	else if (height <= 48 && weight > 180) {
		// document.write("31 &rdquo; ");
		suggested.value = "31\" ";
	}
	else if (height <= 52 && weight <= 90) {
		// document.write("29 &rdquo; ");
		suggested.value = "29\" ";
	}
	else if (height <= 52 && weight <= 140) {
		// document.write("30 &rdquo; ");
		suggested.value = "30\" ";
	}
	else if (height <= 52 && weight > 141) {
		// document.write("31 &rdquo; ");
		suggested.value = "31\" ";
	}
	else if (height <= 56 && weight <= 60) {
		// document.write("29 &rdquo; ");
		suggested.value = "29\" ";
	}
	else if (height <= 56 && weight <= 130) {
		// document.write("30 &rdquo; ");
		suggested.value = "30\" ";
	}
	else if (height <= 56 && weight <= 180) {
		// document.write("31 &rdquo; ");
		suggested.value = "31\" ";
	}
	else if (height <= 56 && weight > 180) {
		// document.write("32 &rdquo; ");
		suggested.value = "32\" ";
	}
	else if (height <= 60 && weight <= 60) {
		// document.write("29 &rdquo; ");
		suggested.value = "29\" ";
	}
	else if (height <= 60 && weight <= 90) {
		// document.write("30 &rdquo; ");
		suggested.value = "30\" ";
	}
	else if (height <= 60 && weight <= 150) {
		// document.write("31 &rdquo; ");
		suggested.value = "31\" ";
	}
	else if (height <= 60 && weight > 151) {
		// document.write("32 &rdquo; ");
		suggested.value = "32\" ";
	}
	else if (height <= 64 && weight <= 60) {
		// document.write("29 &rdquo; ");
		suggested.value = "29\" ";
	}
	else if (height <= 64 && weight <= 70) {
		// document.write("30 &rdquo; ");
		suggested.value = "30\" ";
	}
	else if (height <= 64 && weight <= 120) {
		// document.write("31 &rdquo; ");
		suggested.value = "31\" ";
	}
	else if (height <= 64 && weight <= 170) {
		// document.write("32 &rdquo; ");
		suggested.value = "32\" ";
	}
	else if (height <= 64 && weight > 171) {
		// document.write("33 &rdquo; ");
		suggested.value = "33\" ";
	}
	else if (height <= 68 && weight <= 60) {
		// document.write("30 &rdquo; ");
		suggested.value = "30\" ";
	}
	else if (height <= 68 && weight <= 80) {
		// document.write("31 &rdquo; ");
		suggested.value = "31\" ";
	}
	else if (height <= 68 && weight <= 120) {
		// document.write("32 &rdquo; ");
		suggested.value = "32\" ";
	}
	else if (height <= 68 && weight > 121) {
		// document.write("32 &rdquo; ");
		suggested.value = "33\" ";
	}
	else if (height <= 72 && weight <= 80) {
		// document.write("31 &rdquo; ");
		suggested.value = "31\" ";
	}
	else if (height <= 72 && weight <= 120) {
		// document.write("32 &rdquo; ");
		suggested.value = "32\" ";
	}
	else if (height <= 72 && weight <= 170) {
		// document.write("33 &rdquo; ");
		suggested.value = "33\" ";
	}
	else if (height <= 72 && weight > 171) {
		// document.write("34 &rdquo; ");
		suggested.value = "34\" ";
	}
	else if (height > 72 && weight <= 70) {
		// document.write("31 &rdquo; ");
		suggested.value = "31\" ";
	}
	else if (height > 72 && weight <= 110) {
		// document.write("32 &rdquo; ");
		suggested.value = "32\" ";
	}
	else if (height > 72 && weight <= 150) {
		// document.write("33 &rdquo; ");
		suggested.value = "33\" ";
	}
	else if (height > 72 && weight > 150) {
		// document.write("34 &rdquo; ");
		suggested.value = "34\" ";
	}


}

</script>
