/*
JavaScript Document

FinalVadaszy
10/15/15

Sticky menu control
*/

var $LV = jQuery.noConflict();
//$LV('head').append('<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">');



$LV( document ).ready(function(){

			
			var menu = $LV('#sticky-horizontal-menu');
			var menuContainer = $LV('#menu-container');
			var docked = false;
			
		
				$LV(window).scroll(function() 
				{       
					var init = menuContainer.offset().top;
					var menuWidth = menuContainer.width();

				        if (!docked && (menu.offset().top - $LV("body").scrollTop() < 0)) 
				        {
				            menu.css({
				                position : "fixed",
				                top: 0,
				                width: menuWidth
				            });

				            docked = true;
				        } 
				        else if(docked && $LV("body").scrollTop() <= init)
				        {
				            menu.css({
				                position : "absolute",
				                top: init  + 'px',
				                padding: "0 0"
				            });
				            
				            docked = false;
				        }
				});




})