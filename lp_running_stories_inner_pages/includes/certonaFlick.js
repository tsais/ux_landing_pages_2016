// JavaScript Document
var $j=jQuery.noConflict();
var baseURL = "http://www.sportsauthority.com/product/index.jsp?productId=";

var certGallery; //save instance of flickity gallery
var certGallery2; //save instance of flickity gallery
var sliderWidth;
var sliderWidth;
var buildCount;


function buildCertona(container){

  //wait to see if the json response has loaded
  if(window.jsonResponse.length >= 1){
    var jsonRaw=window.jsonResponse;  
   
    //cycle through response array and find the container id
    for(var z=0; z<window.jsonResponse.length;z++){   
      
      if(window.jsonResponse[z].scheme==container.substring(1)){
        
        //reassign jsonRaw variable to the exact json response for below
        jsonRaw=window.jsonResponse[z].items;      
        
      }    
    }
  
  sliderWidth = $j(container).width();
  buildCount = Math.floor( sliderWidth/150);//how many to show initially

  
  //add html container
  $j(container).html('<div class="gallery"></div>');
  
  //loop through json data array
  for(var k=0;k<jsonRaw.length;k++){ 
    
    //lazy load image version
   $j(container+'>div.gallery').append('<div class="gallery-cell certona"> <a href="' + jsonRaw[k].id + '"> <img class="certona-image gallery-cell-image" data-flickity-lazyload="' + jsonRaw[k].imageurl + '" alt="' + jsonRaw[k].description + '"/></a> <p class="certona-price">$'+ parseInt(jsonRaw[k].listprice).toFixed(2) + '</p> <p class="certona-desc"><a href="' + baseURL+jsonRaw[k].id + '">' + jsonRaw[k].description +'</p></a> <p><a class="certona-buy-now" href="' + baseURL+jsonRaw[k].id + '">Buy Now</a></p> </div>');    
     
  };
  
  //initialize flickity after building slides
  $j(container+'>div.gallery').flickity({ freeScroll: false, contain: true, prevNextButtons: true, pageDots: false, setGallerySize: true, resize: true, autoPlay: false, wrapAround: false, friction: 0.25, imagesLoaded: true, lazyLoad:buildCount, cellAlign: 'left', initialIndex: 0});
  
  //save reference to flickity variable
  certGallery = $j(container+'>div.gallery').flickity();
 
  //show the certona container after everything is built
  //$j(container).css('display','block');
  var index;
  //press next button to skip ahead and load images 
  $j(container+' button.next').on( 'click', function() {
        
    
    //check current index add on next set
    index =$j(container+'>div.gallery').flickity().data('flickity').selectedIndex+(buildCount-1);
       
    //check if at the end
    if(index>$j(container+'>div.gallery').flickity().data('flickity').cells.length){
      index=$j(container+'>div.gallery').flickity().data('flickity').cells.length;
      
    }
    
    
    //move the slider to the new select position
    $j(container+'>div.gallery').flickity('select', index );     
      
  });  

  //previous button same concept as above
  $j(container+' button.previous').on( 'click', function() {
    index =$j(container+'>div.gallery').flickity().data('flickity').selectedIndex-(buildCount-1);
    
    if(index<0){
      index=0;     
    }
    
   $j(container+'>div.gallery').flickity('select', index )
  });  
  
    //show the container
     $j(container).css('visibility','visible');
    
  }else{
    
    setTimeout(function() {buildCertona(container);}, 500);
    
  }/*end of if length*/
  
}; /*end buildCertona*/
  
//After everything on the page has loaded
$j(document).ready(function () {
  "use strict";  

  buildCertona('#lpjson1_rr'); 
  
 
  
});//end document ready  

