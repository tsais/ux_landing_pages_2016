/* 

Luke Vadaszy
Version 2.0
Last Updated: 20160512 jdb

*/


function homePageBuilder(e) 
{  
      
  
 //Connect to Sheet   
  var sheet = SpreadsheetApp.getActiveSheet();
  var data = sheet.getDataRange().getValues();
 
  //Create Constants
  var folderDate = data[0][1];
  var baseURL = "http://www.sportsauthority.com";
  var hostedURL = baseURL + "/graphics/media/tsa/hp/2016/" + folderDate + "/";
  var templateURL = 'http://www.sportsauthority.com/graphics/media/tsa/hp/template_2015/';
  var customerFavs ='<div id="tsaCertona1"> <div class="pure-g max-1220"> <div class="pure-u-1 l-box"> <h2><span>Recommended For You</span></h2></div></div><div class="pure-g max-1220"> <div class="l-box pure-u-1"> <div id="homejson_rr"></div></div></div></div>'; 
  var recomend4You = '<div id="tsaCertona2"> <div class="pure-g max-1220"> <div class="pure-u-1 l-box"> <h2><span>Customer Favorites</span></h2></div></div><div class="max-1220"> <div class="pure-g"> <div class="l-box pure-u-1"> <div id="homejson2_rr"></div></div></div></div></div>';
  
  //Build Arrays
  var slides = [];
  var tsaMinors1 = [];
  var tsaMinors2 = [];
  var tsaMinors3 = [];
  var plCarousel = [];
  var banners =  [];
  var quickLinks = [];
  var brandBar = [];
  var majorCTA = [];
  var slidePad = 55;
    
    
    
  for (var i = 6; i < data.length; i++) {
    //Check Slot
    var slotCheck = data[i][0];
    var sloatCase = slotCheck.toLowerCase();
    
   
    
    switch (true) {
      case (sloatCase.indexOf("major") > -1):
        slides.push([data[i][3],baseURL + data[i][1],data[i][4],data[i][7],data[i][8],data[i][9],data[i][5],data[i][6].split(","),data[i][8].split(",")]);
        break;
      case (sloatCase.indexOf("text deal") > -1):
        tsaMinors1.push([data[i][3],data[i][1],data[i][4]]);
        break;
      case (sloatCase.indexOf("minor") > -1):
        tsaMinors2.push([data[i][3],baseURL + data[i][1],data[i][4]]);
        break;
      case (sloatCase.indexOf("brand") > -1):
        plCarousel.push(["HP-"+data[i][2]+"-PB.jpg",baseURL + data[i][1], data[i][2]]);
        break;
      case (sloatCase.indexOf("magalog") > -1):
        tsaMinors3.push([data[i][3],data[i][1],data[i][4]]);
        break;
      case (sloatCase.indexOf("banner") > -1):
        banners.push([data[i][3],data[i][1],data[i][4],data[i][2]]);
        break;
      case (data[i][2]=="QL"):
        quickLinks.push([data[i][0],baseURL + data[i][1]]);
        break;
      case (data[i][2]=="BB"):
        brandBar.push([data[i][0],baseURL + data[i][1]]);
        break; 
      case (data[i][2]=="SA BB"):
        var shopAllBB = data[i][1];
        break;
       case (data[i][2].indexOf("CTA") > -1):
        majorCTA.push([data[i][2],data[i][5],data[i][6].split(","),data[i][1]]);
        break;  
      default:     
    }
    
  }
    
       
    
  //------------------------------  Assemble HTML  ---------------------------------------------
  //var fullAssembled;
  var scriptsInject = '<script type="text/javascript" src="http://www.sportsauthority.com/graphics/media/tsa/SA/JSLibrary/flickity.pkgd.js"></script><script type="text/javascript" src="http://www.sportsauthority.com/graphics/media/tsa/SA/JSLibrary/jquery.unveil.js"></script><script type="text/javascript" src="http://www.sportsauthority.com/graphics/media/tsa/SA/JSLibrary/initiateHPv2.js"></script><link rel="stylesheet" type="text/css" href="http://www.sportsauthority.com/graphics/media/tsa/SA/css/flickity.css" media="screen" /><link rel="stylesheet" type="text/css" href="http://www.sportsauthority.com/graphics/media/tsa/SA/css/2016_hp.css" /><link rel="stylesheet" href="http://www.sportsauthority.com/graphics/media/tsa/SA/css/pure-min.css"><!--[if lte IE 8]><link rel="stylesheet" href="http://www.sportsauthority.com/graphics/media/tsa/SA/css/grids-responsive-old-ie-min.css"><![endif]--><!--[if gt IE 8]><!--><link rel="stylesheet" href="http://www.sportsauthority.com/graphics/media/tsa/SA/css/grids-responsive-min.css"><!--<![endif]--><link rel="stylesheet" href="http://www.sportsauthority.com/graphics/media/tsa/SA/css/grids-responsive-min.css">';
  
    
  //---- Build Slider -----  
    var slidesGo = "";
    
    if(slides.length >1){
      slidesGo+= '<div id="tsaSlider" class="pure-u-1 gallery">';
    }else{
    slidesGo+= '<div class="pure-u-1">';
    }
    
    for(var m=0;m < slides.length; m++){			
		
      //add slide image
      slidesGo+='<div class="gallery-cell pure-u-1" id="major' + (m+1) + '"><a href="'+ slides[m][1] +'"><img class="pure-u-1" src="' + hostedURL + slides[m][0] + '" alt="'+slides[m][2]+'" /></a>';
      
      // Build first slide
      slidesGo+='<div class="dynoCTA" style="top:'+slides[m][7][0]+'%; left:'+slides[m][7][1]+'%;"><a class="pure-button cta-button sa-'+slides[m][7][2]+'-bg" href="'+ slides[m][1] +'">'+ slides[m][6] +' &#9656;</a></div>';
       
      for(var mc=0;mc < majorCTA.length; mc++){
         if(majorCTA[mc][0].indexOf(m+1) > -1){
           slidesGo+='<div class="dynoCTA" style="top:'+majorCTA[mc][2][0]+'%; left:'+majorCTA[mc][2][1]+'%;"><a class="pure-button cta-button sa-'+majorCTA[mc][2][2]+'-bg" href="'+ majorCTA[mc][3] +'">'+ majorCTA[mc][1] +' &#9656;</a></div>';
         }
        } 
      
      
      if(slides[m][3]){
        slidesGo+='<div class="dynoOffer" style="bottom:'+slides[m][8][0]+'%; left:'+slides[m][8][1]+'%;"><p style="color:'+ slides[m][8][2] +'; font-size:70%;" >'+ slides[m][3] +'&nbsp;<a style="color:'+ slides[m][8][2] +' !important; text-decoration: underline;" href="javascript:window.open(\''+slides[m][5]+'\', \'_blank\',\'width=700,height=500,toolbar=0,menubar=0,location=0,status=0,scrollbars=0,resizable=0,left=0,top=0\')">Offer Details &#187;</a></p></div>';
      }
      slidesGo+='</div>';	
    }
		
    slidesGo+= '</div>';
 //---- End Slider ----  
    
    
 //---- Build Minors ----- 
    var minors1Go = "";
    var minors2Go = "";
    var minors3Go = "";
    
    // Text Deals
    minors1Go+= '<div id="tsaGreatDeals" class="l-box max-1220"> <div class="pure-g"> <div class="pure-u-1 l-box"> <h2><span>Shop Great Deals</span></h2></div></div><div class="pure-g max-1220"> <div id="tsaMinors1" class="pure-u-md-1 pure-u-lg-1 sa-align-mid l-box">';
	
    for(var mn=0; mn<tsaMinors1.length; mn++){		
    	minors1Go+= '<div class="minor pure-u-1 pure-u-sm-1-3 pure-u-md-1-3"><div class="l-box" id="minor1-'+(mn+1)+'" ><a href="'+ tsaMinors1[mn][1] +'"><img class="pure-u-1" src="'+templateURL+'loading-graphic.gif" data-src="' + hostedURL + tsaMinors1[mn][0] + '" alt="'+tsaMinors1[mn][4]+'" /></a><noscript><img class="pure-u-1" src="' + hostedURL + tsaMinors1[mn][0] + '" alt="'+tsaMinors1[mn][2]+'" /></noscript></div></div>';	
	}
    minors1Go+='</div></div>'
   
    // Minors
    minors2Go+= '<div id="tsaTopTrends"> <div class="pure-g max-1220"> <div class="pure-u-1 l-box"> <h2><span>This Week&rsquo;s Top Trends</span></h2></div></div><div class="pure-g max-1220"> <div id="tsaMinors2" class="pure-u-md-1 pure-u-lg-1 sa-align-mid l-box">';
	
    for(var j=0; j < tsaMinors2.length; j++){		
    	minors2Go+='<div class="minor pure-u-1 pure-u-sm-1-2 pure-u-md-1-2"><div class="l-box" id="minor2-'+(j+1)+'" ><a href="' + tsaMinors2[j][1] +'"><img class="pure-u-1" src="'+templateURL+'loading-graphic.gif" data-src="' + hostedURL + tsaMinors2[j][0] + '" alt="'+tsaMinors2[j][2]+'" /></a><noscript><img class="pure-u-1" src="' + hostedURL + tsaMinors2[j][0] + '" alt="'+tsaMinors2[j][2]+'" /></noscript></div></div>';
		
	}
    minors2Go+='</div></div>';
    
	// Catalogs
    minors3Go+= '<div id="tsaCatalog" class="l-box"> <div class="pure-g"> <div class="pure-u-1 l-box"> <h2><span>Spring Highlights</span></h2></div></div><div class="pure-g max-1220"><div id="tsaMinors3" class="pure-u-md-1 pure-u-lg-1 sa-align-mid l-box">';
      
	for(var k=0; k < tsaMinors3.length; k++){		
    	minors3Go+='<div class="minor pure-u-1 pure-u-sm-1-3 pure-u-md-1-3"><div class="l-box" id="minor3-'+(k+1)+'" ><a href="'+ tsaMinors3[k][1] +'"><img class="pure-u-1" src="'+templateURL+'loading-graphic.gif" data-src="' + hostedURL + tsaMinors3[k][0] + '" alt="'+tsaMinors3[k][2]+'" /></a><noscript><img class="pure-u-1" src="' + hostedURL + tsaMinors3[k][0] + '" alt="'+tsaMinors3[k][2]+'" /></noscript></div></div>';
		
	}
    minors3Go+='</div></div></div>';
    
 //---- End Minors ---- 
    
    
 //---- Build Banners ---- 
    var bannerGo1 = "";
    var bannerGo2 = "";
    var bannerGo3 = "";
  
   for(var bn=0; bn < banners.length; bn++){
     
     switch (banners[bn][3]) {
       case 1:
        bannerGo1+='<div id="tsaBanner1" class="tsa-banner"> <div class="pure-g"> <div class="sa-align-mid"><div class="l-box"><a href="'+ banners[bn][1]+'"><img class="pure-u-1" src="'+templateURL+'loading-graphic.gif" data-src="' + hostedURL + banners[bn][0] + '" alt="'+banners[bn][2]+'" /></a></div></div></div></div>';
         break;
       case 2:
        bannerGo2+='<div id="tsaBanner2" class="tsa-banner"><div class="pure-g"> <div class="sa-align-mid"><div class="l-box"><a href="'+ banners[bn][1]+'"><img class="pure-u-1" src="'+templateURL+'loading-graphic.gif" data-src="' + hostedURL + banners[bn][0]  + '" alt="'+banners[bn][2]+'" /></a></div></div></div></div>';
         break;
         case 3:
        bannerGo3+='<div id="tsaBanner3" class="tsa-banner" style="background-color: #f03125;"><div class="pure-g"> <div class="sa-align-mid"><div class="l-box"><a href="'+ banners[bn][1]+'"><img class="pure-u-1" src="'+templateURL+'loading-graphic.gif" data-src="' + templateURL + banners[bn][0]  + '" alt="'+banners[bn][2]+'" /></a></div></div></div></div>';
         break;
   }
    
   }
  if(bannerGo1==""){
    bannerGo1+='<div id="tsaBanner1" class="tsa-banner"> <div class="pure-g"> <div class="sa-align-mid"><div class="l-box"></div></div></div></div>';
  }
   if(bannerGo2==""){
    bannerGo2+='<div id="tsaBanner2" class="tsa-banner"> <div class="pure-g"> <div class="sa-align-mid"><div class="l-box"></div></div></div></div>';
  }
   if(bannerGo3==""){
    bannerGo3+='<div id="tsaBanner3" class="tsa-banner"> <div class="pure-g"> <div class="sa-align-mid"><div class="l-box"></div></div></div></div>';
  }
    
 //---- End Banners ---- 
  
 
//---- Build Private Label ---- 
  var privateLabelGo="";
  
  privateLabelGo+= '<div id="tsaOnly" style="background-color:#ffffff;"> <div class="pure-g"><div class="pure-u-1 l-box"><h2><span>Only At Sports Authority</span></h2></div></div><div><div class="pure-g max-1220"><div id="tsaPrivateLabel" class="pure-u-1 l-box"><div class="gallery pure-u-1">';
    for(var p=0; p < plCarousel.length; p++){			
		
      //add PL Slide
		privateLabelGo+='<div class="gallery-cell"><a href="'+ plCarousel[p][1] +'"><img src="' + templateURL + plCarousel[p][0] + '" alt="'+plCarousel[p][2]+'" /></a></div>';				
	}
		
    privateLabelGo+= '</div></div></div></div></div>';

  
//---- End Private Label ---- 
  
  
   
//---- Build Quick Links ---- 
  var quickLinksGo = "";
  
  quickLinksGo+= '<div id="tsaExplore" style="background-color:#f0f0f0;"> <div class="pure-g"> <div class="pure-u-1"> <h2><span>More to Explore</span></h2></div></div><div class="pure-g max-1220">';
  for(var q=0; q < quickLinks.length; q++){
    
    if( q%6 === 0){
      quickLinksGo+='<div class="pure-u-1 pure-u-sm-1-3 pure-u-md-1-5 pure-u-lg-1-5 list-box"> <div class="l-box"> <h3><a href="'+quickLinks[q][1]+'">'+quickLinks[q][0]+'</a></h3><ul>';
    }else{
    quickLinksGo+='<li><a href="'+quickLinks[q][1]+'">'+quickLinks[q][0]+'</a></li>';
    }
    if( (q+1)%6 === 0 && q!=0){
      quickLinksGo+='</ul></div></div>';
      
    }
  }
 quickLinksGo+='</div></div>';
//---- End Quick Links ---- 
    
  
//---- Build Brand Bars ---- 
  var brandBarGo = ""; 
 
  brandBarGo+='<div style="background-color:#ffffff;"><div id="tsaBrandBar" class="pure-g max-1220"> <div class="gallery pure-u-3-4 pure-u-md-7-8 pure-u-lg-9-10 flickity-enabled is-draggable" tabindex="0">';
    
 for(var bb=0; bb < brandBar.length; bb++){
   brandBarGo+='<div class="gallery-cell pure-u-1-3 pure-u-sm-1-3 pure-u-md-1-6 pure-u-lg-1-10" style="position: absolute; left: 1601px;"> <a href="'+brandBar[bb][1]+'"><img src="http://www.sportsauthority.com/graphics/media/tsa/hp/template_2014/bbar/bb-'+brandBar[bb][0]+'.png" alt="'+brandBar[bb][0]+'"></a> </div>';
 }
  brandBarGo+='</div><div class="gallery-cell pure-u-1-4 pure-u-md-1-8 pure-u-lg-1-10"> <a href="http://www.sportsauthority.com'+shopAllBB+'"><img src="http://www.sportsauthority.com/graphics/media/tsa/hp/template_2014/bbar/bb-shop-all1.png" alt="Shop All Brands"></a></div></div></div>';   
//---- End Brand Bars ----  
  

    
   var fullAssembled = scriptsInject  + '<div id="tsaWrapper" class="bb-pass"><div id="tsaMajor"><div class="l-box max-1220"><div class="pure-g">' + slidesGo + "</div></div></div>" + bannerGo1 + minors1Go + "</div>" + customerFavs + minors2Go+ "</div>" + bannerGo2 + recomend4You + privateLabelGo + brandBarGo + minors3Go + quickLinksGo +bannerGo3+'<div id="tsaServices" class="max-1220"> <div class="pure-g"> <div class="pure-u-1 l-box"> <h2><span>We&rsquo;re Here for You</span></h2></div></div><div class="pure-g outline-box"> <div class="pure-u-1 pure-u-sm-1 pure-u-md-1-2 pure-u-lg-1-2"> <div class="l-box"> <a href="http://stores.sportsauthority.com/"><img class="pure-u-1" src="http://www.sportsauthority.com/graphics/media/tsa/hp/template_2015/HP-FindAStore.jpg" alt=""/></a> <a href="http://stores.sportsauthority.com/">Find a Store Near You &raquo;</a> </div></div><div class="pure-u-1-2 pure-u-sm-1-2 pure-u-md-1-4 pure-u-lg-1-4"> <div class="l-box"> <a href="http://www.sportsauthority.com/shop/index.jsp?categoryId=12455250&amp;ab=MYOAS_HP_Store_Services"><img class="pure-u-1" src="http://www.sportsauthority.com/graphics/media/tsa/hp/template_2015/HP-StoreServies.gif" alt="Store Service Center"/></a> </div></div><div class="pure-u-1-2 pure-u-sm-1-2 pure-u-md-1-4 pure-u-lg-1-4"> <div class="l-box"> <h3><a href="http://www.sportsauthority.com/shop/index.jsp?categoryId=12455250&amp;ab=MYOAS_HP_Store_Services">Store Services</a></h3> <ul> <li>Ski &amp;&nbsp;Board Service &amp;&nbsp;Rental</li><li>Bike Tunes &amp;&nbsp;Services</li><li>Racquet Stringing</li><li>Skate Sharpening</li><li>And Much More!</li></ul> <p class="tiny-text">Services and prices may vary by location.</p><a href="http://www.sportsauthority.com/shop/index.jsp?categoryId=12455250&amp;ab=MYOAS_HP_Store_Services">Learn More &raquo;</a> </div></div></div></div><div class="baseballField"> <div id="tsaVideos" class="max-1220"> <div class="pure-g"> <div class="pure-u-1 l-box"> <h2>MAKE EVERY INNING COUNT</h2></div></div><div class="pure-g"> <div class="pure-u-1 pure-u-sm-1 pure-u-md-1-5"> </div><div id="invodo" class="pure-u-1 pure-u-sm-1 pure-u-md-3-5 pure-u-lg-3-5"> <div class="videoWrapper"> <iframe width="560" height="315" src="https://www.youtube.com/embed/KSLfIHBJVnk?list=PLF4JrUN5fmETVbwLG-PqngI4HzkmIQw8C&amp;rel=0"></iframe> </div></div><div class="pure-u-1 pure-u-sm-1 pure-u-md-1-5"> </div></div></div></div></div><script type="text/javascript" src="http://e.invodo.com/4.0/s/sportsauthority.com.js"></script><script type="text/javascript" src="http://www.sportsauthority.com/js/resxclsx.js"></script><script type="text/javascript">/*take certona response store json for later*/ var jsonResponse=[]; function loadjson(data){for (var i=0; i < data.resonance.schemes.length; i++){jsonResponse.push(data.resonance.schemes[i]);}}var resx=new Object(); resx.appid="THESPORTSAUTHORITY01"; resx.top1=100000; resx.top2=100000; resx.lkmatch=/productid%3d\d+/i; resx.customerid=""; resx.rrec=true; resx.rrelem="homejson_rr;homejson2_rr"; resx.rrnum="15"; resx.rrcall="loadjson"; certonaResx.run();</script>';
   var demoFile = '<!doctype html><html><head><title>Home Page Demo Page</title><meta charset="utf-8"></head><body><script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>'+fullAssembled+'</body></html>';
    

    
//Build readable date
  var currentDateGo="";
  var currentDateRaw = new Date();
  currentDateGo += currentDateRaw.getMonth()+1;
  currentDateGo += currentDateRaw.getDate();
  currentDateGo += "_" + currentDateRaw.getHours(); 
  currentDateGo += currentDateRaw.getMinutes();
  
  
// Create New HTML in drive
//DriveApp.getRootFolder().createFile('HP-GSI-'+folderDate+'-'+currentDateGo,fullAssembled, MimeType.HTML);   
//DriveApp.getRootFolder().createFile('HP-QA-'+folderDate+'-'+currentDateGo,demoFile, MimeType.HTML);  
 
 //DriveApp.createFile('HP-GSI-'+folderDate+'-'+currentDateGo,fullAssembled, MimeType.HTML).makeCopy('HP-GSI-'+folderDate+'-'+currentDateGo,"Homepages");
  
  var fileGSI = DriveApp.createFile('HP-GSI-'+folderDate+'-'+currentDateGo,fullAssembled, MimeType.HTML);
  var fileQA = DriveApp.createFile('HP-QA-'+folderDate+'-'+currentDateGo,demoFile, MimeType.HTML);
  var folders = DriveApp.getFoldersByName("Hompages");
  var testFolder = folders.next();
  fileGSI.makeCopy('HP-GSI-'+folderDate+'-'+currentDateGo, testFolder);
  fileQA.makeCopy('HP-QA-'+folderDate+'-'+currentDateGo, testFolder);

}