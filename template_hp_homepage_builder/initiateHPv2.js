/* updated 20160512 1533 jdb */
var $h=jQuery.noConflict(); 
function buildCertona(container){
  
  //wait to see if the json response has loaded
  if(window.jsonResponse.length >= 1){
    var jsonRaw=window.jsonResponse;  
   
    //cycle through response array and find the container id
    for(var z=0; z<window.jsonResponse.length;z++){   
      
      if(window.jsonResponse[z].scheme==container.substring(1)){
        
        //reassign jsonRaw variable to the exact json response for below
        jsonRaw=window.jsonResponse[z].items;      
        
      }    
    }


    var pidURL = "http://www.sportsauthority.com/product/index.jsp?productId=";

    var sliderWidth;
    var sliderWidth;
    var buildCount;
    var listPrice;
    var salePrice;
    var showListPrice=true; 

    sliderWidth = $h(container).width();
    buildCount = Math.floor( sliderWidth/150);//how many to show initially  


    //add html container
    $h(container).html('<div class="gallery"></div>');

    //loop through json data array
    for(var k=0;k<jsonRaw.length;k++){ 

      listPrice=parseFloat(jsonRaw[k].listprice).toFixed(2);
      salePrice=parseFloat(jsonRaw[k].saleprice).toFixed(2);

      if(showListPrice && salePrice<listPrice){
        $h(container+'>div.gallery').append('<div class="gallery-cell certona"> <a href="' + pidURL+jsonRaw[k].id + '"> <img class="certona-image gallery-cell-image" data-flickity-lazyload="' + jsonRaw[k].imageurl + '" alt="' + jsonRaw[k].description + '"/></a> <p class="certona-price"><span class="certona-sale-price">$'+ salePrice + '</span> <span class="certona-list-price">$'+ listPrice + '</span></p> <p class="certona-desc"><a href="' + pidURL+jsonRaw[k].id + '">' + jsonRaw[k].description +'</p></a></div>');

      }else{
     $h(container+'>div.gallery').append('<div class="gallery-cell certona"> <a href="' + pidURL+jsonRaw[k].id + '"> <img class="certona-image gallery-cell-image" data-flickity-lazyload="' + jsonRaw[k].imageurl + '" alt="' + jsonRaw[k].description + '"/></a> <p class="certona-price">$'+ parseInt(jsonRaw[k].saleprice).toFixed(2) + '</p> <p class="certona-desc"><a href="' + pidURL+jsonRaw[k].id + '">' + jsonRaw[k].description +'</p></a></div>');
      }

    };//end loop building carousel

    //initialize flickity after building slides
    $h(container+'>div.gallery').flickity({ freeScroll: true, contain: true, prevNextButtons: true, pageDots: false, setGallerySize: false, resize: true, autoPlay: false, wrapAround: false, friction: 0.25, imagesLoaded: true, lazyLoad:buildCount, cellAlign: 'left', initialIndex: 0});

    var index;

    //press next button to skip ahead and load images 
    $h(container+' button.next').on( 'click', function() {        

      //check current index add on next set
      index =$h(container+'>div.gallery').flickity().data('flickity').selectedIndex+(buildCount-1);

      //check if at the end
      if(index>$h(container+'>div.gallery').flickity().data('flickity').cells.length){
        index=$h(container+'>div.gallery').flickity().data('flickity').cells.length;
      }

      //move the slider to the new select position
      $h(container+'>div.gallery').flickity('select', index ); 
    });/*end next button function*/

    //previous button same concept as above
    $h(container+' button.previous').on( 'click', function() {
      index =$h(container+'>div.gallery').flickity().data('flickity').selectedIndex-(buildCount-1);

      if(index<0){index=0;}

     $h(container+'>div.gallery').flickity('select', index )
    });/*end prev button function*/  


    //show the container
     $h(container).css('visibility','visible');
    
  }else{
    
    setTimeout(function() {buildCertona(container);}, 500);
    
  }/*end of if length*/
  
}; /*end buildCertona*/

function loadInvodo(){
	Invodo.init({
  pageName: "HomePage",
  pageType: "product",
  onload: function(){
	   Invodo.Widget.add({refId:"fieldVid1", widgetId:"player1", mode:"embedded", type:"inplayer", parentDomId:"fieldVid1", autoplay:false, podId:"TRBG50M8"});
	   }
	});
	
}

// Functions for building CTA's and Offer Copy
function buildCTA(divID,ctaText,ctaTop,ctaLeft,ctaColor,ctaURL){
$h('#'+divID).append('<div class="pover" style="top:'+ctaTop+'%; left:'+ctaLeft+'%;"><a class="pure-button cta-button sa-'+ ctaColor +'-bg" href="'+ ctaURL +'">'+ ctaText +' &#9656</a>');
}

function buildOffer(divID,copyColor,ctaText,ctaTop,ctaLeft,ctaColor,ctaURL){
$h('#'+divID).append('<div class="pover" style="top:'+ctaTop+'%; left:'+ctaLeft+'%; color:'+ copyColor +'"><p style="color:'+ copyColor +' font-size:70%;" >'+ ctaText +'<a style="color:'+ ctaColor +'; text-decoration: underline;" href="'+ ctaURL +'">Offer Details &#187;</a></p>');
}
  
$h(document).ready(function () {
  "use strict";
  //initialize flickity Sliders
  if($h(".gallery-cell","#tsaSlider").length > 1){
    $h('#tsaSlider').flickity({ freeScroll: false, contain: false, prevNextButtons: true, pageDots: true, setGallerySize: false, resize: true, autoPlay: 7000, wrapAround: true, friction: 0.25,imagesLoaded: true});
  }
    $h('#tsaPrivateLabel>div').flickity({ freeScroll: false, contain: false, prevNextButtons: true, pageDots: false, setGallerySize: false, resize: true, autoPlay: false, wrapAround: true, friction: 0.25, lazyLoad:5, imagesLoaded: true});
    $h('#tsaBrandBar div.gallery').flickity({ freeScroll: false, contain: true, prevNextButtons: false, pageDots: false, setGallerySize: false, resize: true, autoPlay: false, wrapAround: true, friction: 0.25, cellAlign: 'left', percentPosition: false,  imagesLoaded: true});

    
    //make image maps respond to hover
    $h('area').mouseover(function(event) { 
      $h('#tsaSlider .flickity-prev-next-button.previous').addClass('hover');
      $h('#tsaSlider .flickity-prev-next-button.next').addClass('hover');
    });
    $h('area').mouseout(function(event) {
      $h('#tsaSlider .flickity-prev-next-button.previous').removeClass('hover');
      $h('#tsaSlider .flickity-prev-next-button.next').removeClass('hover');
    });
    
    $h('img').unveil();
    buildCertona('#homejson_rr'); 
    buildCertona('#homejson2_rr');


    //Add media query
    var ctaCount = $h('div.dynoCTA').length;
	var slideCount = $h('#tsaSlider .gallery-cell').length;
	
    if( (slideCount > 1) && (ctaCount > 2) ){
      $h("head").append('<style>@media screen and (max-width: 569px){#tsaWrapper #tsaSlider {padding-bottom: 65%;}}</style>');
    }else{
      $h("head").append('<style>@media screen and (max-width: 569px){#tsaWrapper #tsaSlider {padding-bottom: 53%;}}</style>');
    }

  });

  













