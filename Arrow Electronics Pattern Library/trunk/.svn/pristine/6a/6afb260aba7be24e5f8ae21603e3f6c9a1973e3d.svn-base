//
// Buttons
//

//== Button Row
//== Button Sizes
//== Cancel Button
//== Primary Button
//== Secondary Button
//== Tertiary Button




%button {
	@include border-radius();
	color: #FFF;
	cursor: pointer;
	border: 1px solid;
	font: {
		family: $font-family-base;
		size: $font-size-base;
		weight: normal;
	}
	padding: $spacing-half $spacing-base;

	&:focus { outline: 1px dotted $gray-tint-25 }

	.fa { margin-right: $spacing-half }
}

%split-button {
	position: relative;

	button {
		border-radius: 0;
		float: left;
		margin: 0;
		position: relative;

		&:first-of-type { @include border-left-radius() }
		&:last-of-type { @include border-right-radius() }
	}

	.button-menu {
		@include border-bottom-radius();
		@include box-shadow();
		background: #FFF;
		border: 1px solid $border-color-base;
		display: none;
		overflow: hidden;
		padding: 0;
		position: absolute;
		right: -1px;
		top: 1.8rem;
		width: 10rem;
		z-index: 999;

		ul {
			@extend %reset-list;

			li {
				border-bottom: 1px solid $border-color-base;

				a {
					display: block;
					padding: $spacing-base;
				}

				&:last-child { border-bottom: none }
			}
		}
	}
}




//==  Button Row
//

.button-row {
	float: right;
	padding: $spacing-2x 0 $spacing-base 0;

	button {
		float: left;
		margin-right: $spacing-2x;

		&:last-child { margin-right: 0 }

		&.primary-button { margin-top: -$spacing-half }
	}
}




//== Button Sizes
//

.button-200 {
	font-size: $font-size-200;
	padding: $spacing-base $spacing-2x;
}




//== Cancel Button
//

.cancel-button {
	@extend %button;
	background: #FFF;
	border: none;
	color: $link-color;
	padding-left: 0;
	padding-right: 0;

	&:hover { color: $link-color-hover }

	.icon {
		@extend .fa-times;
		color: $alert-color-indicator;
		margin-right: $spacing-half;
	}
}




//== Primary Button
//

.primary-button {
	@extend %button;
	background: $primary-btn-color;
	border-color: $primary-btn-outline;
	font-size: $font-size-200;	
	padding: $spacing-base $spacing-2x;

	.fa { color: $green-shade-25 }
}




//== Secondary Button
//

.secondary-button {
	@extend %button;
	background: $secondary-btn-color;
	border-color: $secondary-btn-outline;
}




//== Tertiary Button
//

.tertiary-button {
	@extend %button;
	background: $tertiary-btn-color;
	border-color: $tertiary-btn-outline;
	color: $text-color-base;
}

