/*  All classes in this file are prefaced with .global-body so that
    they will not interfere with the same tag or class names used in other
    header/shell CSS files.
*/

.global-body header .header-color {
    height: 3px;

    &:nth-child(1) { background-color: $header-color-1; }
    &:nth-child(2) { background-color: $header-color-2; }
    &:nth-child(3) { background-color: $header-color-3; }
    &:nth-child(4) { background-color: $header-color-4; }
    &:nth-child(5) { background-color: $header-color-5; }
    &:nth-child(6) { background-color: $header-color-6; }
    &:nth-child(7) { background-color: $header-color-7; }
    &:nth-child(8) { background-color: $header-color-8; }
    &:nth-child(9) { background-color: $header-color-9; }
    &:nth-child(10) { background-color: $header-color-10; }
}

.global-body header .main-header {

    .logo-section {
        border-right: 1px solid rgba(255, 255, 255, .2);
    }
}

.global-body header .first-row {
    border-bottom: 1px solid $gray-tint-10;

    .settings-profile {

        .fa-gears {
            border-right: 1px solid $gray-tint-10;
        }

        .message-alert {
            @include border-radius(2px);

            padding: 1px 3px;
            top: -4px;
        }
    }
}

// First tier menu items that when hovered, expand
// a menu below

.global-body header ul.menu-dropdown {
    border-right: 1px solid $gray-base;
    list-style-type: none;

    li.first-tier {
        @include transition(all, .3s);
        color: $gray-tint-75;
    }

    &:hover {
        background-color: $gray-shade-50;

        li.first-tier {
            // background-color: $gray-shade-50;
            color: white;

            // This is here so that its default behavior of
            // being hidden is overridden when the parent is
            // hovered over.
            ul.expanded-menu {
                opacity: 1;

                li {
                    display: block;
                }
            }
        }
    }
}

// These are the second tier menu items that expand
// below the first tier dropdown

.global-body header ul.expanded-menu {
    @include transition(all, .3s);
    @include transition-delay(.2s);

    // custom dual-shadow for open menus
    -webkit-box-shadow: 0 1px 4px rgba(0, 0, 0, .5),
                        0 1px 12px rgba(0, 0, 0, .2);
    -moz-box-shadow:    0 1px 4px rgba(0, 0, 0, .5),
                        0 1px 12px rgba(0, 0, 0, .2);
    box-shadow:         0 1px 4px rgba(0, 0, 0, .5),
                        0 1px 12px rgba(0, 0, 0, .2);

    border-bottom-left-radius: 3px;
    border-bottom-right-radius: 3px;
    left: 0;
    list-style-type: none;
    min-width: 250px;
    opacity: 0;
    padding: 0;
    top: 32px;
    z-index: 2;

    li {
        @include justify-content(flex-start);
        @include transition(all, .2s);
        background-color: $gray-shade-50;
        color: white;
        display: none;
        font-size: 12px;

        &:hover {
            background-color: $gray-tint-75;
            color: $gray-shade-50;
        }

        &:last-child {
            border-bottom-left-radius: 3px;
            border-bottom-right-radius: 3px;
        }
    }
}

.global-body header .second-row {
    border-top: 1px solid $gray-tint-25;
}

.global-body header .search-container {
    border-right: 1px solid rgba(255, 255, 255, .2);

    ul {
        list-style-type: none;

        li {
            background-color: $gray-tint-35;
            font-size: 10px;
        }

        li:first-child {
            border: none;
            border-bottom-left-radius: 3px;
            border-top-left-radius: 3px;
            color: $gray-tint-90;
            font-size: 10px;
            height: 25px;
            padding-top: 7px;

            .fa-caret-down {
                color: $gray-base;
                font-size: 12px;
            }
        }
    }

    .search-button {
        background-color: $blue-base;
        border: none;
        border-bottom-right-radius: 3px;
        border-top-right-radius: 3px;
        color: white;
        font-size: 12px;
        height: 25px;
    }

    .search-input {
        border: none;
        font-size: 12px;
        width: 50%;
    }
}

.global-body header .tertiary-button-container {

    button {
        @include transition(all, .3s);
        border: none;
        border-right: 1px solid rgba(255, 255, 255, .2);
        color: $gray-tint-90;

        &:hover {
            &:nth-child(1) { color: $menu-color-1; }
            &:nth-child(2) { color: $menu-color-2; }
            &:nth-child(3) { color: $menu-color-3; }
            &:nth-child(4) { color: $menu-color-4; }
            &:nth-child(5) { color: $menu-color-5; }
            &:nth-child(6) { color: $menu-color-6; }
            &:nth-child(7) { color: $menu-color-7; }
        }

        span {
            display: block;
        }

        .caption {
            font-size: 10px;
        }

        .fa {
            font-size: 16px;
        }
    }
}
