// prevents jquery not found errors if jquery loads after this file
$(document).ready(function(){

	//navigation
	$('.navicon').on('click', function() {
		$('.global-navigation nav').toggleClass('active');
	});
	$('.global-navigation li.drop-down-menu').on('click', function() {
		$this = $(this);
		$('.global-navigation li').not($this).removeClass('active');
		$this.toggleClass('active');
	});
	$('.account li.drop-down-menu').on('click', function() {
		$this = $(this);
		$('.account li.drop-down-menu').not($this).removeClass('active');
		$this.toggleClass('active');
	});
	$('.context-menu').on('click', function() {
		$(this).children('ul').toggleClass('active');
	});

	//horizontal-accordion
	$('.accordion-title').on('click', function() {
		$this = $(this);
		if(!$this.parent().hasClass('active')) {
			$('.horizontal-accordion .accordion-section').removeClass('active');
			$this.parent().addClass('active');
		}
	});

	//tooltip
	$('.tooltip').on('click', function() {
		$(this).children('.tip-info').toggleClass('display-block');
	});
	$('.popover-link').on('click', function() {

		$(this).toggleClass('show-popover');
	})

	//split-secondary-btn
	$('.split-secondary-btn button:nth-child(2)').on('click', function() {
		$('.split-secondary-btn .btn-menu').toggleClass('display-block');
	});
	$('.split-secondary-btn .btn-menu li').on('click', function() {
		$('.split-secondary-btn .btn-menu').toggleClass('display-block');
	});

	//table drawer
	$('.toggle-drawer a').on('click', function() {
		$(this).parent().parent().toggleClass('drawer-expand');
	});

	// Using a toggle function prevents binding/unbinding issues caused by nested
	// .on('click') rebindings.
	// Function is also generic and can accept any expand or collapse class names.
	var toggleExpandCollapse = function(context, expandClass, collapseClass) {
				alert('hi');
		// These variables are only truthy if the parent class was found.
		var expanded = $(context).parent('.' + expandClass)[0];
		var collapsed = $(context).parent('.' + collapseClass)[0];

		// The if/else serves as the actual toggle
		if (expanded) {
			$(context).parent().removeClass(expandClass).addClass(collapseClass);
		} else if (collapsed) {
			$(context).parent().removeClass(collapseClass).addClass(expandClass);
		}
	}

	//expand / collapse content box
	$('.expanded .box-title, .collapsed .box-title').on('click', function() {
		toggleExpandCollapse(this, 'expanded', 'collapsed');
	});


	//lightboxes
	$('.launch-lightbox').on('click', function() {
		var lightboxID = $(this).attr('id');

		$('.lightbox-container').addClass('launch-lightbox');
		setTimeout(function() {
			$('#' + lightboxID + 'Lightbox').toggleClass('launch-lightbox');
		}, 50);
	});
	$('.close-lightbox').on('click', function() {
		$(this).closest('.right-lightbox').removeClass('launch-lightbox');
		$(this).closest('.top-lightbox').removeClass('launch-lightbox');
		if(!$('.right-lightbox').hasClass('launch-lightbox')) {
			setTimeout(function() {
				$('.lightbox-container').removeClass('launch-lightbox');
			}, 200);
		}
	});

	//auto-suggest
	$('.auto-suggest').on('click', 'input', function() {
		$(this).parent().children('ul').toggleClass('display-block');
	});

	// spinner
	$('button.primary-button').on('click', function(){
	    $('.spinner-lightbox').toggleClass('launch-spinner');
	});

	// mobile drawer
	$('.drawer-handle').on('click', function(){
	    $(this).parent().toggleClass('active');
	});

	// tab-box
	$('.tab-set .tab').on('click', function() {
		var tabboxID = $(this).attr('id');
		$(this).siblings('.tab').removeClass('active');
		$(this).addClass('active');
		$(this).closest('.horizontal-tabs, .horizontal-tabs-flex').find('.box-content').removeClass('active');
		$('#' + tabboxID + '-content').addClass('active');
	});

	// complex-input
	$('.complex-input .dropdown-menu').on('click', function() {
		$(this).toggleClass('open');
	});

});





























// Sortable
/* HTML5 Sortable (http://farhadi.ir/projects/html5sortable)
 * Released under the MIT license.
 */(function(a){var b,c=a();a.fn.sortable=function(d){var e=String(d);return d=a.extend({connectWith:!1},d),this.each(function(){if(/^enable|disable|destroy$/.test(e)){var f=a(this).children(a(this).data("items")).attr("draggable",e=="enable");e=="destroy"&&f.add(this).removeData("connectWith items").off("dragstart.h5s dragend.h5s selectstart.h5s dragover.h5s dragenter.h5s drop.h5s");return}var g,h,f=a(this).children(d.items),i=a("<"+(/^ul|ol$/i.test(this.tagName)?"li":"div")+' class="sortable-placeholder">');f.find(d.handle).mousedown(function(){g=!0}).mouseup(function(){g=!1}),a(this).data("items",d.items),c=c.add(i),d.connectWith&&a(d.connectWith).add(this).data("connectWith",d.connectWith),f.attr("draggable","true").on("dragstart.h5s",function(c){if(d.handle&&!g)return!1;g=!1;var e=c.originalEvent.dataTransfer;e.effectAllowed="move",e.setData("Text","dummy"),h=(b=a(this)).addClass("sortable-dragging").index()}).on("dragend.h5s",function(){b.removeClass("sortable-dragging").show(),c.detach(),h!=b.index()&&f.parent().trigger("sortupdate",{item:b}),b=null}).not("a[href], img").on("selectstart.h5s",function(){return this.dragDrop&&this.dragDrop(),!1}).end().add([this,i]).on("dragover.h5s dragenter.h5s drop.h5s",function(e){return!f.is(b)&&d.connectWith!==a(b).parent().data("connectWith")?!0:e.type=="drop"?(e.stopPropagation(),c.filter(":visible").after(b),!1):(e.preventDefault(),e.originalEvent.dataTransfer.dropEffect="move",f.is(this)?(d.forcePlaceholderSize&&i.height(b.outerHeight()),b.hide(),a(this)[i.index()<a(this).index()?"after":"before"](i),c.not(i).detach()):!c.is(this)&&!a(this).children(d.items).length&&(c.detach(),a(this).append(i)),!1)})})}})(jQuery);

// Drag and Drop
$(document).ready(function(){
    // var newList = true;
    var theList = document.getElementById('theList');

    $('.sortable').sortable().bind('sortupdate', function(){
        //sessionStorage.setItem('todoListPlus',theList.innerHTML)
    });

    /*
     *
     *
     *           Commented code below is for local/session storage
     *
     *
     */

    //$('#theList').on('change', '.listItem', function(){
    //    currentValue = $(this).val();
    //    $(this).attr('value', currentValue);
    //
    //    sessionStorage.setItem('todoListPlus',theList.innerHTML);
    //});

    //$('#theList').on('click','.removeListItem', function(e){
        //e.preventDefault();
        //$(this).parent().remove();

        //sessionStorage.setItem('todoListPlus',theList.innerHTML)
    //});

    //loadToDo();
    //
    //function loadToDo() {
    //    if(sessionStorage.getItem('todoListPlus')) {
    //        theList.innerHTML = sessionStorage.getItem('todoListPlus');
    //
    //        $('.sortable').sortable('destroy');
    //        $('.sortable').sortable({
    //            handle: '.handle'
    //        });
    //
    //    }
    //}

});
