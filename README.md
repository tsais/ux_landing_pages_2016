# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

*This is a folder to hold all our UX projects including landing pages (lp), custom category pages (ccp), brand shops (bs), email templates (email), and utilities (ut)

### How do I get set up? ###

* Clone the repository to your local machine (recommend withing htdocs folder so you can use WAMP)
* Create new project folders within
* Push your changes/commits at the end of every day
* Pull all updates at the start of each day


### Contribution guidelines ###

* Keep consistent naming convention for your project folders (see above for project type) ask if something doesn't exist.


### Who do I talk to? ###

* Talk with John Burrows